# elastic_search

## What is elastic search?

1. Real time distributed and analytics engine.
2. It's open source and developed in java.
3. Elastic search is based on the lucene engine on top of which we have a rest interface.
4. Support full text search as it's document based.
5. Used for single page application.

## Why Elastic Search?
1. Let's you perform and combine many types of searches like structured, unstructured, geo, metrics etc.
2. You can ask a query anyway you want.
3. Let's you understand billions of log lines easily.
4. Provide aggregationwhich help you zoom out to explore trends and patterns in your data.

## Advantages
1. scalable
2. Fast
3. Multilingual
4. Document oriented
5. Autocompletion and instant search
6. schema free

## Installation
* Download and run bin/elastic_search.bat in windows

## Baisc concepts
1. Near real time: It's a near realtime platform that there is a slight from the time you index a document until the time it 
becomes searchable.
2. Cluster: A cluster is a collection of one or multiple nodes that together holds the entire data. It provides federated indexing and search capabilities across nodes and is identified by a unique name. (by default it is elasticsearch).
3. A node is a single server which is a part of cluster, store data and participate in the cluster's indexing and search capabilities.
4. Index: An index is collection of documents with similar characterastics and is identified by a name. This name is used to refer to the index while performing indexing, search, update and delete against a document in it.
5. Type: A type is a logical category/partition of an index whose semantics is completly. It is defined for documents that have a set of common fields. You can define more than one type in your index.
6. Document: A document is a basic unit of information which can be indexed. It's expressed in json which is an ubiquitos internet data interchange format.
7. Shards: Elastic search provides the ability to subdivide index into multiple peices called Shards. Each shard is in itself a fully functional and independent index that can be hosted on any node within the cluster.
8. Replicas: Elastic search allows you make one or more copies of your index's shard which are called replica shard or replica. 

## API conventions
The elastic search APIs are accessed using json over HTTP. It uses the following conventions through out the rest API.
1. Multiple indices: Most API support execution across multiple indices like comma separated noation, wildcard notations like *,+,-. Url query string parameters like 'ignore_unavailable', 'allow_no_indices', 'expand_wildcards'.
2. Date math support in index name: It let's you to search indices according to date and time. You need to specify date time in a specific format like <name{date_math_expr{date format | timezone}}>.
3. common options: Following are some common options for all the REST APIs, like: pretty results, human readable output, date math, response filtering, flat settings, parameter, no values, Time units, Byte size units, unit less quantities, distance units, fuzzyness, enabling stack traces, request body in query string.
4. URL based access control: Users can also use a proxy with url based access control to secure access to elastic search indices. User has an option of specifying an index in the URL, and on each individual request with request objects like: multi search, multi get, bulk.

## Elastic Search APIs - CRUD operation

1. Document API: Performs operation on docuemnt label. These are 2 types: first one is, Single document API: Index API, GET API, Update API, delete API. Another one is multi document api which includes Multiget API, Bulk API, Delete by query API, Update by Query, reindex API. Single document api is used to query a single document and multiple api is used to work with multi document api.

        a. Index API: creating a document. 

            Put /index_name/document_name/id_ 

            {
                "title":"a",
                "artist": "B"
            }



        b. Get API: Getting the document like

            GET /index_name/document_name/id_ 

        c. Update API: Call via PUT method and add parameter.

            PUT /index_name/document_name/id_ 

            {
                "title":"a",
                "artist":"b",
                "source":"X"
            }

        d. Delete API: To delete the document

            DELETE /index_name/document_name/id_ 

2. Search API: Using search API you can execute a search query and get back search hits that match the query. Search can be done by the following ways:

    a. Multi index: You can search for the documents present in all the indices or in some specific indices.
    b. Multi type: You can search all documents in an index across all types or in specified types.
    c. URI search: Various parameters can be passed in a search operation using URI, like: q, lenient, fields, sort, timeout,
       terminate after, from, size.


        GET /index_name/document_name?q=year=2017 


3. Aggregation API: Aggregation collects all the data which is selected by the search query. This framework consists up of many building blocks called aggregators, which help in building complex summaries of data.

        "aggregations" : {
            "<aggregation_name>":{
                "<aggregation_type>":{
                    <aggregation_body>
                }
                [,"meta":{ [<meta_data_body>] } ]?
                [,"aggregations":{ ['<sub_aggregation>']+ } ]?

            }
            [, "<aggregation_name_2>" : { ... } ]*
        }

There are 4 types of aggregation, these are: bucket, metric, matrix, pipeline. Suppose the following query is to find out all the student whose first name is "satya"

    GET document_name
    {
        "query": {
            "match":{
                "first_name":"satya"
            }
        }
    }

Another query from a bank to list all the customers whose bank balance is between 0 to 1000.

    GET document_name
    {
        "query":{
            "range":{
                "balance":{
                    "gte":0,
                    "lte":1000
                }
            }
        }
    }


4. Index API: Index API are responsible for managinf, all the aspects of index like settings, aliases, mappings, index templates.
Some of the commands are: Ceate Index, Delete Index, Get Index, Index exits, Open/Close index APIS, Index aliases, Index Settings, Analyze, Index template, Index stats, Flush, Refresh.

5. Cluster API: Use for getting information about cluster and it's nodes and making changes in them. Like: Cluster Health, Cluster state, Cluster stats, Pending Cluster task, Cluster reroute, Node stats, Nodes_hot_threads.

## Query DSL
Elastic search provides a full query DSL based on json to define queries. Query DSL is an AST of queries consists of two types of clauses, those are: Leaf query clause, compound query clause. Leaf query clause searches for a value in a particular entry. But compound query clause combine leaf query clause or other query clause like multiple leaf structure. 

## mapping
Mapping is the process of defining how a document and the fields that it contains and are stored and indexed. Mapping types: a. meta fields, fields or properties. Field data types are: Core data types, Geo data types, complex data types, specialized data types. 

Mapping parameters: 

## Analysis
During a search operation when a query is processed , the content in any index is analyzed by analysis module. The followings are part of analysis
1. Analyzer: It converts the fields into tokens and with the help of token filter we do further operations.
2. Tokenizer
3. Token filter
4. character filter

## Modules
Elastic search consists of 2 kinds of modules, which are responsible for it's functionality. first one is static setting, these settings need to be configured in in the config file (elasticsearch.yml) before starting elasticsearch. Second one is dymanic setting, these settings can be set on live elastic search. 

## module types
