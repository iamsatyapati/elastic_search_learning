1. /* 1. Create an index hiking (Use default setting of 5 shards and 1 replica) */

PUT hiking
 
2. Add one or more documents in the index created consisting the following fields:
    * hiked_on (with date value of format yyyy/MM/dd, example “2019/01/26”)
    * coordinator (with string value, example “Jane Smith”)
    * total_students (with integer value, example 12)
    * cost (with floating point value, example 3456.67)
    * bus_booked (with boolean, example true/false)
    * checklist (with array of string, example [“a”,”b”])
    * test_field (any kind of value you want)
*/

POST hiking/_doc/1
{
    "hiked_on":"11th may, 2021",
    "coordinator":"Satya",
    "total_students":12,
    "cost":50.5,
    "bus_booked":true,
    "checklist":['tiffin box', 'water bottle'],
    "contact_no":"+91 9438xxxxxx"
}

/* 3. Practice retrieving documents by various methods shown in examples. */

GET hiking/_doc/1

GET hiking/_doc/_search

/* Get source directly */
GET hiking/_doc/1/_source

/* Exclude source */
GET hiking/_doc/1?source=false

/* Get selected fields*/
GET hiking/_doc/1?_source_include=coordinator, total_students

/* 4. Update any inserted document by adding a new field without using script. (Related to normal update) */
POST hiking/_doc/1/_update
{
    "doc":{
        "difficulty":"moderate"
    }
}

/* 5. Update any inserted document to add a new field and remove field added in (4) at the same time using script.
(Related to scripted update)
*/

POST hiking/_doc/1/_update
{
    "script": {
        "lang": "painless",
        "source": """
            ctx._source.level = 3; 
            ctx._source.remove('difficulty')
        """
    }
}

6. Update any field of any inserted document (say total_students) to change its value. Make use of params. 
(Related to scripted update)
*/

POST hiking/_doc/1/_update
{
    "script":{
        "lang":"painless"
         "source": "ctx._source.total_students = params.total_students",
         "params":{
             "total_students":21
         }
    }
}

/*7. Use loop and condition to carry out any operation in any inserted document (Related to scripted update)*/
POST hiking/_doc/1/_update
{
    "script":{
        "lang":"painless",
        "source":"""
        for(int t=0; t<3; t++){
            String item = params.checklist[i];
                if (!ctx._source.checklist.contains(item)) {
                    ctx._source.checklist.add(item)
        }
    }""",
    "params": {
            "checklist": ["Camera", "Water Bottle", "Whistle"]
        }
    }
}
