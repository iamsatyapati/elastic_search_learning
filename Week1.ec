/* creating an index */
PUT test_index

/* creating an index with a document in it */
PUT test_index/_doc/1
{
    "name": "document name"
}

/*creating a document using POST*/
POST test_index/_doc/1
{
    "name":"document name"
}

/*Reading a document */
GET test_index/_doc/1

/*All document by search*/
GET test_index/_search

/*update document*/
POST test_index/_doc/1/_update
{
    "name":"cerebronics"
}

/*using scripts*/
POST test_index/_doc/1/_update
{
    "script":"ctx._source.stock+=10"
}

/*using painless script*/
POST test_index/_doc/1/_update
{
    "script":{
        "lang":"painless",
        "source": "ctx._source.stock-=params.val",
        "params"{
            "val":5
        }
    }
}

/*check if update field exists*/
POST test_index/_doc/1/_update
{
    "script":{
        "lang":"painless",
        "source": """
              if (ctx._source.stock != null) {
                ctx._source.stock += params.val;
              }""",
        "params":{
            "val":5
        }
    }
}

/*Adding a new field without script*/
POST test_index/_doc/5
{
    "tags":[
        "technology"
    ]
}

/*Adding array object as a field*/
POST test_index/_doc/5/_update
{
    "script":{
        "lang":"painless",
        "source":"ctx._source.tags.addAll(params.tag)",
        "params":{
            "tag":["abc"]
        }
    }
}

/*delete a document*/
POST test_index/_doc/5/_update
{
    "script":{
        "lang":"painless",
        "course": """
              if (ctx._source.tags.contains(params.tag)) {
                ctx.op = 'delete'
              }
    """,
    "params":{
            "tag":"technology"
        }
    }
}
